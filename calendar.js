

var currentMonth = new Month(2016, 3);
var selectedCell = null;
$(document).ready(function () {

	updateCalendar();

	$("#next_month").click(function (){
		currentMonth = currentMonth.nextMonth();
		updateCalendar();
	});

	$("#prev_month").click(function (){
		currentMonth = currentMonth.prevMonth();
		updateCalendar();
	});


	$("#calendar td").click(function()
	{
		var date = $(this).attr("value");
		if(date!=null){
		openEventDialog();
		}
	});


});
function openEventDialog() {
	$("#mydialog").dialog({
	autoOpen : false,
    height: 280,
    width: 480,

});
	$("#mydialog").dialog("open");
}


function updateCalendar(){

	$("#today").html(monthToString(currentMonth.month)+ " " + currentMonth.year);
	var lastMonth = currentMonth.prevMonth();
	var weeks = currentMonth.getWeeks();
 
	for(var w in weeks){
		var days = weeks[w].getDates();
		for(var d in days){
			var today = $(".week" + w).find(".day"+d);
			if(days[d].getDate()==1)
			{ 
				if(w==0)
				{
				today.html("<strong>"+monthToString(currentMonth.month)+"</strong>"+" "+days[d].getDate());
				}
				else{
				today.html("<strong>"+monthToString(currentMonth.nextMonth().month)+"</strong>"+" "+days[d].getDate());
				theMonth = 1;
				}
			}
			else{
		    today.html(days[d].getDate());}

		   	if(w==0&&days[d].getDate()>7)
		   	{
		   		var date = monthToString(currentMonth.prevMonth().month)+"-"+days[d].getDate()+"-"+currentMonth.prevMonth().year;
		   	}else if(w==4&&days[d].getDate()<=7)
		   	{
		   		var date = monthToString(currentMonth.nextMonth().month)+"-"+days[d].getDate()+"-"+currentMonth.nextMonth().year;
		   	}
		   	else{
				var date = monthToString(currentMonth.month)+"-"+days[d].getDate()+"-"+currentMonth.year;
		   	}

		   	$("#day").attr("value",date);
		   	fetchEvents();
		   	var eventCell = document.getElementById("day");
		   	today.html(today.html()+"<br/>"+eventCell.innerHTML);
		}

	}
	
}

function addEvent(event){


	var eventname = $("#eventname").val();
	var time = document.getElementById("hour").options[document.getElementById("hour").selectedIndex].value;
	var day = document.getElementById("day").value;

	var dataString = "eventname=" + encodeURIComponent(eventname) + "&day=" + encodeURIComponent(day) + "&time=" + encodeURIComponent(time);

	var xmlHttp = new XMLHttpRequest(); 
	xmlHttp.open("POST", "events.php", true); 
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    xmlHttp.send(dataString);
    xmlHttp.addEventListener("load", addEventCallback, false);
	
	$("#eventname").val("");
	$("#day").val("");

}

function addEventCallback(event) {

    var jsonData = JSON.parse(event.target.responseText); 
    if(jsonData.eventAdded){
        updateCalendar();}
}

function editEventAjax(event){

	var eventname = $("#eventname").val();
	var hour = document.getElementById("hour").options[document.getElementById("hour").selectedIndex].value;
	var day = document.getElementById("day").value;
	
    
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "eventname=" + encodeURIComponent(eventname) + "&day=" + encodeURIComponent(day) + "&hour=" + encodeURIComponent(hour);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "editEvent.php", true); 
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
        xmlHttp.send(dataString); // Send the data
        xmlHttp.addEventListener("load", editEventCallback, false);
	
	$("#eventname").val("");
	$("#day").val("");
}

function editEventCallback(event) {

    var jsonData = JSON.parse(event.target.responseText); 
    if(jsonData.eventEdit){
        var eventname = jsonData.eventname;
	$("#event").hide();
    }
    else{
        console.log(jsonData.message);
    }
}

function fetchEvents(event) {
    
    var day = document.getElementById("day").value;
    var dataString = "day=" + encodeURIComponent(day);
    
    
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "getEvents.php",true);
    xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", fetchEventsCallback, false);
    xmlHttp.send(dataString);
}

function fetchEventsCallback(event) {

    var jsonData = JSON.parse(event.target.responseText);
    var cell = document.getElementById("day");
    cell.innerHTML = "";
    if(jsonData.eventExisted){
	var events = jsonData.events;
	for(var i in events) {
	    var title = events[i].title;
	    var time = events[i].time;
	    cell.innerHTML += "<p>" + title + " " + time + "</p>" + "<br>"; 
	}
        
    }else{
	console.log(jsonData.message);
    }
}

function monthToString(month) {
    switch(month) {
	case 0:
	  return "January";
	  break;
	case 1:
	  return "February";
	  break;
	case 2:
	  return "March";
	  break;
	case 3:
	  return "April";
	  break;
	case 4:
	  return "May";
	  break;
	case 5:
	  return "June";
	  break;
	case 6:
	  return "July";
	  break;
	case 7:
	  return "August";
	  break;
	case 8:
	  return "September";
	  break;
	case 9:
	  return "October";
	  break;
	case 10:
	  return "November";
	  break;
	case 11:
	  return "December";
	  break;
	default:
	  return "Unknown Month";
    }
}








