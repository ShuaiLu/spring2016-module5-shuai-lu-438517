<?php
session_start();
require 'database.php';
header("Content-Type:application/json");

$username = $_POST['user'];
$password = $_POST['pass'];
$stmt = $mysqli->prepare("select username,password from users where username=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('s', $username);
$stmt->execute();
$stmt->bind_result($first, $last);
$stmt->fetch();

    if(crypt($password,$last)==$last)
    {
     $_SESSION['username']=$first;
     $_SESSION['token']= substr(md5(rand()),0,10);
     
     echo json_encode(array(
     "success" => true,
     "message" => "Welcome ".$first)
     );
     exit();
    }
    else{
    	echo json_encode(
    		"success" => false,
    		"message" => "username does not exist or wrong password"
    	);
    	exit();
    }

?>