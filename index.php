<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="calendar.css" />
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/start/jquery-ui.css" type="text/css" rel="Stylesheet" /> 
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js"></script>
    <script src="calendar.min.js"></script>
    <script src="calendar.js"></script>
    <script src="login.js"></script>
    <title>My Calendar</title>
  
</head>

<body>
    
    <h2>My calendar</h2>

<?php
session_start();
include('database.php');
?>

<div id="login" class="login" >  
  <h3>Login</h3>
  <label>Username:</label>
  <input id="username" type="text" name="username"/><br/>
  <label>Password:</label>
  <input id="password" type="password" name="password"/><br/>
   
    <button id="login_btn">login</button>
    <button id="signUp_btn">Sign up</button>
</div>


<div id="loginMessage" class="login">
    <button id="logout_btn">logout</button>
</div>

<div id="event" class="login">
  <div id="existingEvents"></div>
  <label>Event:</label>
  <input id="eventname" type="text" name="eventname" ><br/>
  <input type="hidden" id="day" value="">

  <select id="hour" name="hour">
  <option selected="selected" value="0:00:00">0:00</option>
  <option value="1:00:00">1:00</option>
  <option value="2:00:00">2:00</option>
  <option value="3:00:00">3:00</option>
  <option value="4:00:00">4:00</option>
  <option value="5:00:00">5:00</option>
  <option value="6:00:00">6:00</option>
  <option value="7:00:00">7:00</option>
  <option value="8:00:00">8:00</option>
  <option value="9:00:00">9:00</option>
  <option value="10:00:00">10:00</option>
  <option value="11:00:00">11:00</option>
  <option value="12:00:00">12:00</option>
  <option value="13:00:00">13:00</option>
  <option value="14:00:00">14:00</option>
  <option value="15:00:00">15:00</option>
  <option value="16:00:00">16:00</option>
  <option value="17:00:00">17:00</option>
  <option value="18:00:00">18:00</option>
  <option value="19:00:00">19:00</option>
  <option value="20:00:00">20:00</option>
  <option value="21:00:00">21:00</option>
  <option value="22:00:00">22:00</option>
  <option value="23:00:00">23:00</option>
  </select>



  <input type="hidden" id="token" name="token" value="<?php echo $_SESSION['token'];?>" />
  <button id="new_event_btn">Create</button>
  <button id="delete_event_btn">Delete</button>
  <button id="edit_event_btn">Edit</button>
</div>
<div id="mydialog" title="Howdy" style="display:none">Look at me!</div>
<div id="calendar">
    <h1 id="today">Mar 2016</h1>
    <button id="prev_month">perv</button>
    <button id="next_month">next</button>
    <table id="calendarTable" class="calendar">
  <tr class="days">
    <td class="mon">Sunday</td>
    <td class="tue">Tuesday</td>
    <td class="wed">Wednesday</td>
    <td class="mon">Thursday</td>
    <td class="mon">Friday</td>
    <td class="mon">Saturday</td>
    <td class="mon">Monday</td>
  </tr>
	<tr class="week0">
		<td class="day0" value=""></td>
		<td class="day1" value=""></td>
		<td class="day2" value=""></td>
		<td class="day3" value=""></td>
		<td class="day4" value=""></td>
		<td class="day5" value=""></td>
		<td class="day6" value=""></td>
	</tr>
	<tr class="week1">
		<td class="day0" value=""></td>
		<td class="day1" value=""></td>
		<td class="day2" value=""></td>
		<td class="day3" value=""></td>
		<td class="day4" value=""></td>
		<td class="day5" value=""></td>
		<td class="day6" value=""></td>
	</tr>
	<tr class="week2">
		<td class="day0" value=""></td>
		<td class="day1" value=""></td>
		<td class="day2" value=""></td>
		<td class="day3" value=""></td>
		<td class="day4" value=""></td>
		<td class="day5" value=""></td>
		<td class="day6" value=""></td>
	</tr>
	<tr class="week3">
		<td class="day0" value=""></td>
		<td class="day1" value=""></td>
		<td class="day2" value=""></td>
		<td class="day3" value=""></td>
		<td class="day4" value=""></td>
		<td class="day5" value=""></td>
		<td class="day6" value=""></td>
	</tr>
	<tr class="week4">
		<td class="day0" value=""></td>
		<td class="day1" value=""></td>
		<td class="day2" value=""></td>
		<td class="day3" value=""></td>
		<td class="day4" value=""></td>
		<td class="day5" value=""></td>
		<td class="day6" value=""></td>
	</tr>
</table>
</div>


</body>
</html>