<?php
include "database.php";
header("Content-Type: application/json");
session_start();
ini_set("session.cookie_httponly", 1);
$username = $_SESSION['username'];
$day = mysql_real_escape_string(htmlentities( $_POST["day"]) );
$stmt = $mysqli->prepare("select title,time from events where username=? and time=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('ss', $username,$day);
$stmt->execute();
$result = mysql_fetch_array($res);
$events = array();
while($event = mysql_fetch_assoc($res)) {
    $events[] = $event;
    
}
if($events != null) {
    echo json_encode(array(
            "eventExisted" => true,
            "events" => $events    
    ));
    exit();
} else {
    echo json_encode(array(
            "eventExisted" => false,
	    	"message" => "Event not exist"
    ));
    exit();
}
    
?>